import re
import sys
import time
#from os import path
from PyQt5.QtWidgets import QMessageBox, QMainWindow, QFileDialog, QTableWidgetItem, QApplication
#from PyQt5.uic import *

#FORM_CLASS, _ = loadUiType(path.join(path.dirname(__file__), "main.ui"))


#class MainApp(QMainWindow, FORM_CLASS):
from main import Ui_virement

class MainApp(QMainWindow, Ui_virement):
    def __init__(self, parent=None):
        super(MainApp, self).__init__(parent)
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.UI()
        self.save_reloadtab()
        self.click_generer()
        self.chemin = ""
        self.listrib = []
        self.chemin_d = ""
        self.mois = ""
        self.genfichier = ""
        self.completed = int
        self.contenu_source = ""
        self.nom_soc = ""
        self.menu.addAction("By Hatem Besbes")

    def UI(self):
        self.setWindowTitle('Ordre de Virement BNA')
        self.setFixedSize(800, 610)

    def reinitialiser(self):
        self.setFixedSize(800, 610)
        self.e_source_file.clear()
        self.e_compte_b.clear()
        self.e_dest_file.clear()
        self.e_nbr_emp.setValue(1)
        self.listWidget.setCurrentItem(None)
        self.progressBar.setValue(0)

    def file_browse(self):
        self.progressBar.setValue(0)
        chemin = QFileDialog.getOpenFileName(self, caption='ouvrir', directory='.', filter='Fichiers texte (*.txt *.csv)')
        text = str(chemin)
        self.chemin = (text[2:].split(',')[0].replace("'", ""))
        self.e_source_file.setText(self.chemin)
        try:
            with open("%s" % self.chemin, 'r') as f1:
                file = f1.readlines()  # [1:]  # enregister le fichier source dans des lignes
                self.contenu_source = str(file)
                self.setFixedSize(1330, 610)
                self.contenu_f_source.setText(self.contenu_source)
        except IOError:
            return

    def file_browse_dest(self):
        self.progressBar.setValue(0)
        chemin = QFileDialog.getExistingDirectory(self, caption='Enregistrer dans ..')
        self.chemin_d = str(chemin)
        self.e_dest_file.setText(self.chemin_d)

    def click_generer(self):
        self.generer.clicked.connect(self.file_generator)
        self.Parcourir.clicked.connect(self.file_browse)
        self.reset.clicked.connect(self.reinitialiser)
        self.Parcourir_dest.clicked.connect(self.file_browse_dest)
        self.quitter_app.clicked.connect(self.quitter)
        self.actionNouveau.triggered.connect(self.reinitialiser)
        self.actionQuitter.triggered.connect(self.quitter)
        self.enr_nouv_rib.clicked.connect(self.save_reloadtab)
        self.ajouter_rib.clicked.connect(self.select_rib)

    def select_rib(self):
        self.progressBar.setValue(0)
        self.nom_soc = str(self.l_rib.currentItem())
        with open("rib.txt", "r") as f:
            text = str(f.read())
            listrib = text.replace('\n', '').split(',')
            if self.nom_soc != 'None':
                self.nom_soc = self.l_rib.currentItem().text()
                if self.nom_soc in listrib:
                    ind = listrib.index(self.nom_soc)
                    compte_soc = listrib[ind+1]
                    self.e_compte_b.setText(compte_soc)
                else:
                    QMessageBox.warning(self, "Message d'erreur", "Compte Invalide")
                    return
            else:
               QMessageBox.warning(self, 'Message d erreur', 'Prière de selectionner RIB de la liste des RIB')

    def save_reloadtab(self):                                                  #charger les donées depuis le fichier text rib.txt
        self.tableWidget.clearContents()
        self.progressBar.setValue(0)
        with open("rib.txt", "r") as f:
             text = str(f.read())
             self.listrib = text.replace('\n', '').split(',')
             n = len(self.listrib)
             i = 0                                                              #indice nom societe
             j = 1                                                              #indice rib
             z = 0
             while i < n and j + 1 < n:                                         #parcourir la liste et affecter les nom et les rib consecutivement au colone de la widget table
                self.tableWidget.setItem(z, 0, QTableWidgetItem(self.listrib[i])) # inserer dans la tablewidget colonne 0 = nomsociete
                self.tableWidget.setItem(z, 1, QTableWidgetItem(self.listrib[j]))
                i = i + 2
                j = j + 2
                z = z + 1
        x = int(self.tableWidget.rowCount())                                     #affecterà la liste l_rib les nom de société de la table widget
        self.l_rib.clear()
        for col in range(x):
            try:
                self.l_rib.insertItem(col, self.tableWidget.item(col, 0).text()) #il faut ajouter .text() pour la convertir en str
            except:
                continue

    def file_generator(self):
        self.progressBar.setValue(0)
        ligne1 = '11010000030032020000011788000000000'
        ordre_de_virement = time.strftime("-%Y-%m-%d-%H-%M-%S")  # le nom de fichier qui va etre générer
        debut = '110100000030220200000217880000000000'
        compte_soc = self.e_compte_b.text()
        fin = '0'.zfill(16).ljust(31, ' ')
        num_ligne = 0
        if self.chemin[-4:] not in ('.txt', '.csv'):
            QMessageBox.warning(self, "Message Erreur", "Veuillez recharger le fichier texte")
            return
        if self.chemin_d == '':
            QMessageBox.warning(self, "Message Erreur", "Veuillez entrer un dossier de destination")
            return
        if self.listWidget.currentItem() is None:
            QMessageBox.warning(self, "Message d'Erreur", "Prière de selectionner le mois de la paie")
            return
        else:
            self.mois = self.listWidget.currentItem().text()
            vir_mois = ('000Virement Salaire %s 2020' % self.mois)
            
        if len(self.e_compte_b.text()) != 20 or re.match("[-+]?\d+$", compte_soc) is None:                              # la ligne ne contenant que des chiffres
            QMessageBox.warning(self, "Message Erreur", "Veuillez entrer un compte bancaire valide")
        else:
            compte_soc = self.e_compte_b.text()
            with open("%s" % self.chemin, 'r') as f1, open(
                    "%s\Ordre_de_Virement%s.txt" % (self.chemin_d, ordre_de_virement),
                    'w+') as f:  # ouvrir le fichier source en mode read et le fichier qui va etre générer en mode write
                file = f1.readlines()  # [1:]  # enregister le fichier source dans des lignes
                total_vir = 0
                for j in file[1:]:  # pour faire le total du montant à virer
                    x = (j[1:10].replace(" ", "").replace(",", ""))
                    total_vir = total_vir + int(x)
                ligne_emp = ""
                for i in file[1:]:  # parcourir le fichier
                    vir_mois.ljust(80)
                    
                    nom_emp = i[34:57].replace('"', '').replace(" ", "").replace(";", "").replace("\n", "")
                    if self.checkBox.isChecked():
                        nom_employe = nom_emp.ljust(30)
                        virement_mois = vir_mois.ljust(76)
                    else:
                        nom_employe = nom_emp.ljust(50)
                        virement_mois = vir_mois.ljust(80)
                    num_ligne = num_ligne + 1
                    # list prendra la nouvelle valeur de chaque ligne
                    file[0] = ligne1 + str(total_vir).zfill(8) + str(num_ligne).zfill(7)
                    list = debut + i[1:10].replace(" ", "").replace(",", "").zfill(7) + \
                    (str(num_ligne)).zfill(7) + compte_soc + i[12:33][1:3] + i[12:33][4:7] + i[12:33].replace(
                    '"', '').replace(";", "").zfill(20) + nom_employe + virement_mois + fin
                    ligne_emp = ligne_emp + ('%s\n' % list)  # écrire dans le fichier destination
                ligne_emp[-1].replace('\n', '')
                ligne_tot = str(ligne1 + str(total_vir).zfill(8) + str(num_ligne).zfill(7)) + '\n'
                self.genfichier = ligne_tot + ligne_emp[:-1]
                if self.e_nbr_emp.value() != num_ligne:
                    QMessageBox.warning(self, "Message Erreur",
                                        "le nombre d'employés saisi ne correspond pas le fichier"
                                        "!! Le nombre d'employé dans le fichier est %s, Prière de vérifier" % num_ligne)
                    maj = QMessageBox.question(self, 'Mettre à Jour', "Voulez-vous mettre à jour le nombre d'employé à %s ?" % num_ligne,
                    QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
                    if maj == QMessageBox.Yes:
                        self.e_nbr_emp.setValue(num_ligne)                
                    else:
                        return
                    return
                f.write('%s' % self.genfichier)
                self.contenu_f_dest.setText(self.genfichier)
                self.setFixedSize(1330, 920)
                self.progress()
                QMessageBox.information(self, "Résultat de la Génération", "Fichier Générer avec Succés")
                #self.chemin = ''

    def progress(self):
        self.completed = 0
        while self.completed < 100:
            self.completed += 0.0005
            self.progressBar.setValue(self.completed)

    def quitter(self):
        quitter_button = QMessageBox.question(self, 'Quitter', "Voulez-vous vraiment Quitter ?",
                                              QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if quitter_button == QMessageBox.Yes:
            exit()
        else:
            return


def main():
    app = QApplication(sys.argv)
    window = MainApp()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
